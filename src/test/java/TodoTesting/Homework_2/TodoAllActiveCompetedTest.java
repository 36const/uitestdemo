package TodoTesting.Homework_2;

import com.codeborne.selenide.ElementsCollection;
import org.junit.Test;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.*;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class TodoAllActiveCompetedTest extends OpenPageAndClearDataAfterEachTest {

    ElementsCollection tasks = $$("#todo-list li");

    @Step
    public void addTask(String text) {
        $("#new-todo").val(text).pressEnter();
    }

    @Step
    public void toggleTask(String taskText) {
        tasks.find(text(taskText)).find(".toggle").click();
    }

    @Step
    public void filterAll(){ $(By.linkText("All")).click(); }

    @Step
    public void filterActive(){ $(By.linkText("Active")).click(); }

    @Step
    public void filterCompleted(){ $(By.linkText("Completed")).click(); }

    @Step
    public void assertItemsLeftCounter(int itemsNumber) {
        $("#todo-count>strong").shouldHave(exactText(String.valueOf(itemsNumber)));
    }

    @Step
    public void clearCompleted() {
        $("#clear-completed").click();
    }

    @Step
    public void toggleAll() {
        $("#toggle-all").click();
    }

    @Step
    public void editTask(String oldText, String newText) {
        tasks.find(exactText(oldText)).find("label").doubleClick();
        tasks.find(cssClass("editing")).find(".edit").setValue(newText).pressEnter();
    }

    @Step
    public void destroyTask(String destroyText) {
        tasks.find(exactText(destroyText)).hover().find(".destroy").click();
    }

    @Test
    public void testAllFilter() {

        // Precondition
        addTask("a");
        addTask("b");
        addTask("c");
        addTask("d");
        tasks.shouldHave(exactTexts("a", "b", "c", "d"));
        assertItemsLeftCounter(4);

        // Editing
        editTask("a", "a edition");
        tasks.shouldHave(exactTexts("a edition", "b", "c", "d"));

        // Deleting by X
        destroyTask("b");
        tasks.shouldHave(exactTexts("a edition", "c", "d"));

        // Marking
        toggleTask("c");
        tasks.filter(cssClass("completed")).shouldHave(exactTexts("c"));
        assertItemsLeftCounter(2);

        // Deleting by clear-completed
        clearCompleted();
        tasks.shouldHave(exactTexts("a edition", "d"));

        // Reopening
        addTask("c for reopening");
        toggleTask("c for reopening");
        assertItemsLeftCounter(2);
        toggleTask("c for reopening");
        tasks.shouldHave(exactTexts("a edition", "d", "c for reopening"));
        assertItemsLeftCounter(3);

        toggleAll();
        clearCompleted();
        tasks.shouldBe(empty);
    }

    @Test
    public void testActiveFilter() {

        // Precondition
        addTask("a");
        addTask("b");
        addTask("c");
        toggleTask("c");
        tasks.filter(visible).shouldHave(exactTexts("a", "b", "c"));

        filterActive();
        tasks.filter(visible).shouldHave(texts("a", "b"));

        // Adding task
        addTask("d from active");
        tasks.filter(visible).shouldHave(texts("a", "b", "d from active"));
        assertItemsLeftCounter(3);

        // Visibility
        filterAll();
        tasks.filter(visible).shouldHave(texts("a", "b", "c", "d from active"));
        assertItemsLeftCounter(3);

        filterActive();
        tasks.filter(visible).shouldHave(texts("a", "b", "d from active"));

        // Editing
        editTask("a", "a edition");
        assertItemsLeftCounter(3);

        // Deleting by clear-completed
        clearCompleted();
        tasks.filter(visible).shouldHave(texts("a edition", "b", "d from active"));
        assertItemsLeftCounter(3);

        // Deleting by X
        destroyTask("b");
        tasks.shouldHave(exactTexts("a edition", "d from active"));

        toggleAll();
        tasks.filter(visible).shouldBe(empty);
        assertItemsLeftCounter(0);
    }

    @Test
    public void testCompletedFilter() {

        // Precondition
        addTask("a");
        addTask("b");
        addTask("c");
        toggleTask("c");
        tasks.filter(visible).shouldHave(exactTexts("a", "b", "c"));

        // Visibility
        filterCompleted();
        tasks.filter(visible).shouldHave(exactTexts("c"));
        assertItemsLeftCounter(2);

        filterActive();
        addTask("d from completed");
        toggleTask("d from completed");
        filterCompleted();
        tasks.filter(visible).shouldHave(texts("c", "d from completed"));
        assertItemsLeftCounter(2);

        // Deleting by X
        destroyTask("c");
        tasks.filter(visible).shouldHave(exactTexts("d from completed"));
        assertItemsLeftCounter(2);

        // Deleting by clear-completed
        $("#clear-completed").click();
        tasks.filter(visible).shouldBe(empty);
        assertItemsLeftCounter(2);

        filterAll();
        tasks.shouldHave(exactTexts("a", "b"));
        assertItemsLeftCounter(2);
    }
}