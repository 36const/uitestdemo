package TodoTesting.Homework_2;

import com.codeborne.selenide.Screenshots;
import com.google.common.io.Files;
import org.junit.After;
import ru.yandex.qatools.allure.annotations.Attachment;

import java.io.File;
import java.io.IOException;

public class ScreenshotCreatingPerTest {

    @After      // creates screenshot after each test
    public void tearDown() throws IOException {
        // void - the function does not return any value
        screenshot(); // if there is () after name - it is a method
    }

    @Attachment(type = "image/png")     // in () - annotation parameters
                                        // type = "image/png" - type of attachment in test report
    public byte[] screenshot() throws IOException {
        // byte - the function returns a value with byte type
        // [] - there will be array of value
        File screenshot = Screenshots.getScreenShotAsFile();
        return Files.toByteArray(screenshot);
    }
}
