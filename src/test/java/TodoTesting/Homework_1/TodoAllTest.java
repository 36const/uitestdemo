package ua.net.itlabs.Homework_1;

import com.codeborne.selenide.ElementsCollection;
import org.junit.Test;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.cssClass;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;

public class TodoAllTest {

    ElementsCollection tasks = $$("#todo-list li");

    public void addTask(String text) {
        $("#new-todo").val(text).pressEnter();
    }

    @Test
    public void testCreateDeleteCheckboxTask() {

        open("http://todomvc.com/examples/troopjs_require/#/");

        addTask("a");
        addTask("b");
        addTask("c");
        addTask("d");
        tasks.shouldHave(exactTexts("a", "b", "c", "d"));

        // deleting by X
        tasks.find(text("b")).hover().find(".destroyTask").click();
        tasks.shouldHave(exactTexts("a", "c", "d"));

        // marking
        tasks.find(text("d")).find(".toggle").click();
        tasks.filter(cssClass("completed")).shouldHave(exactTexts("d"));

        // deleting by clear-completed
        $("#clear-completed").click();
        tasks.shouldHave(exactTexts("a", "c"));

        $("#toggle-all").click();
        $("#clear-completed").click();
        tasks.shouldBe(empty);
    }
}
