package TodoTesting.Homework_4.pages;

import com.codeborne.selenide.ElementsCollection;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class TodoMVC {

    public static ElementsCollection tasks = $$("#todo-list li");

    @Step
    public static void addTask(String text) {
        $("#new-todo").val(text).pressEnter();
    }

    @Step
    public static void toggleTask(String taskText) {
        tasks.find(text(taskText)).find(".toggle").click();
    }

    @Step
    public static void filterAll(){ $(By.linkText("All")).click(); }

    @Step
    public static void filterActive(){ $(By.linkText("Active")).click(); }

    @Step
    public static void filterCompleted(){ $(By.linkText("Completed")).click(); }

    @Step
    public static void assertItemsLeftCounter(int itemsNumber) {
        $("#todo-count>strong").shouldHave(exactText(String.valueOf(itemsNumber)));
    }

    @Step
    public static void clearCompleted() {
        $("#clear-completed").click();
    }

    @Step
    public static void toggleAll() {
        $("#toggle-all").click();
    }

    @Step
    public static void editTask(String oldText, String newText) {
        tasks.find(exactText(oldText)).find("label").doubleClick();
        tasks.find(cssClass("editing")).find(".edit").setValue(newText).pressEnter();
    }

    @Step
    public static void destroyTask(String destroyText) {
        tasks.find(exactText(destroyText)).hover().find(".destroy").click();
    }
}
