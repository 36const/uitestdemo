package TodoTesting.Homework_4;

import org.junit.Test;

import static com.codeborne.selenide.CollectionCondition.*;
import static com.codeborne.selenide.Condition.cssClass;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static TodoTesting.Homework_4.pages.TodoMVC.*;

public class TodoAllActiveCompetedTest extends OpenPageAndClearDataAfterEachTest {

    @Test
    public void testAllFilter() {

        // Creating
        addTask("a");
        addTask("b");
        addTask("c");
        addTask("d");
        tasks.shouldHave(exactTexts("a", "b", "c", "d"));
        assertItemsLeftCounter(4);

        // Editing
        editTask("a", "a edition");
        tasks.shouldHave(exactTexts("a edition", "b", "c", "d"));

        // Deleting by X
        destroyTask("b");
        tasks.shouldHave(exactTexts("a edition", "c", "d"));

        // Marking
        toggleTask("c");
        tasks.filter(cssClass("completed")).shouldHave(exactTexts("c"));
        assertItemsLeftCounter(2);

        // Deleting by clear-completed
        clearCompleted();
        tasks.shouldHave(exactTexts("a edition", "d"));

        // Reopening
        addTask("c for reopening");
        toggleTask("c for reopening");
        assertItemsLeftCounter(2);
        toggleTask("c for reopening");
        tasks.shouldHave(exactTexts("a edition", "d", "c for reopening"));
        assertItemsLeftCounter(3);

        toggleAll();
        clearCompleted();
        tasks.shouldBe(empty);
    }

    @Test
    public void testActiveFilter() {

        // Creating
        addTask("a");
        addTask("b");
        addTask("c");
        toggleTask("c");
        tasks.filter(visible).shouldHave(exactTexts("a", "b", "c"));

        // Only active-items are visible
        filterActive();
        tasks.filter(visible).shouldHave(texts("a", "b"));

        // Adding task from active-bookmarks
        addTask("d from active");
        tasks.filter(visible).shouldHave(texts("a", "b", "d from active"));
        assertItemsLeftCounter(3);

        // All items are visible
        filterAll();
        tasks.filter(visible).shouldHave(texts("a", "b", "c", "d from active"));
        assertItemsLeftCounter(3);

        // Only active-items are visible
        filterActive();
        tasks.filter(visible).shouldHave(texts("a", "b", "d from active"));

        // Editing
        editTask("a", "a edition");
        tasks.filter(visible).shouldHave(texts("a edition", "b", "d from active"));
        filterAll();
        tasks.filter(visible).shouldHave(texts("a edition", "b", "c", "d from active"));

        // Active-button list remained the same after clear-completed
        filterActive();
        clearCompleted();
        tasks.filter(visible).shouldHave(exactTexts("a edition", "b", "d from active"));
        assertItemsLeftCounter(3);

        // Deleting by X
        destroyTask("b");
        tasks.shouldHave(exactTexts("a edition", "d from active"));

        toggleAll();
        tasks.filter(visible).shouldBe(empty);
        assertItemsLeftCounter(0);
    }

    @Test
    public void testCompletedFilter() {

        // Creating
        addTask("a");
        addTask("b");
        addTask("c");
        toggleTask("c");
        tasks.filter(visible).shouldHave(exactTexts("a", "b", "c"));

        // Only completed items are visible
        filterCompleted();
        tasks.filter(visible).shouldHave(exactTexts("c"));
        assertItemsLeftCounter(2);

        // Toggled task in Active also is visible in Completed
        filterActive();
        addTask("d from completed");
        toggleTask("d from completed");
        filterCompleted();
        tasks.filter(visible).shouldHave(texts("c", "d from completed"));
        assertItemsLeftCounter(2);

        // Deleting by X
        destroyTask("c");
        tasks.filter(visible).shouldHave(exactTexts("d from completed"));
        assertItemsLeftCounter(2);

        // Deleting by clear-completed
        $("#clear-completed").click();
        tasks.filter(visible).shouldBe(empty);
        assertItemsLeftCounter(2);

        // There are not deleted items in All
        filterAll();
        tasks.shouldHave(exactTexts("a", "b"));
        assertItemsLeftCounter(2);
    }
}