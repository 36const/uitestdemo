package TodoTesting.Homework_3.pageobjects;

import org.junit.Test;
import TodoTesting.Homework_3.pageobjects.pages.TodoMVCPage;

import static com.codeborne.selenide.CollectionCondition.*;
import static com.codeborne.selenide.Condition.cssClass;
import static com.codeborne.selenide.Condition.visible;

public class TodoAllActiveCompetedTest extends OpenPageAndClearDataAfterEachTest {

    TodoMVCPage page = new TodoMVCPage();

    @Test
    public void testAllFilter() {

        // Precondition
        page.addTask("a");
        page.addTask("b");
        page.addTask("c");
        page.addTask("d");
        page.tasks.shouldHave(exactTexts("a", "b", "c", "d"));
        page.assertItemsLeftCounter(4);

        // Editing
        page.editTask("a", "a edition");
        page.tasks.shouldHave(exactTexts("a edition", "b", "c", "d"));

        // Deleting by X
        page.destroyTask("b");
        page.tasks.shouldHave(exactTexts("a edition", "c", "d"));

        // Marking
        page.toggleTask("c");
        page.tasks.filter(cssClass("completed")).shouldHave(exactTexts("c"));
        page.assertItemsLeftCounter(2);

        // Deleting by clear-completed
        page.clearCompleted();
        page.tasks.shouldHave(exactTexts("a edition", "d"));

        // Reopening
        page.addTask("c for reopening");
        page.toggleTask("c for reopening");
        page.assertItemsLeftCounter(2);
        page.toggleTask("c for reopening");
        page.tasks.shouldHave(exactTexts("a edition", "d", "c for reopening"));
        page.assertItemsLeftCounter(3);

        page.toggleAll();
        page.clearCompleted();
        page.tasks.shouldBe(empty);
    }

    @Test
    public void testActiveFilter() {

        // Precondition
        page.addTask("a");
        page.addTask("b");
        page.addTask("c");
        page.toggleTask("c");
        page.tasks.filter(visible).shouldHave(exactTexts("a", "b", "c"));

        // Only active-items are visible
        page.filterActive();
        page.tasks.filter(visible).shouldHave(texts("a", "b"));

        // Adding task from active-bookmarks
        page.addTask("d from active");
        page.tasks.filter(visible).shouldHave(texts("a", "b", "d from active"));
        page.assertItemsLeftCounter(3);

        // All items are visible
        page.filterAll();
        page.tasks.filter(visible).shouldHave(texts("a", "b", "c", "d from active"));
        page.assertItemsLeftCounter(3);

        // Only active-items are visible
        page.filterActive();
        page.tasks.filter(visible).shouldHave(texts("a", "b", "d from active"));

        // Editing
        page.editTask("a", "a edition");

        // Active-button list remained the same after clear-completed
        page.clearCompleted();
        page.tasks.filter(visible).shouldHave(texts("a edition", "b", "d from active"));
        page.assertItemsLeftCounter(3);

        // Deleting by X
        page.destroyTask("b");
        page.tasks.shouldHave(exactTexts("a edition", "d from active"));

        page.toggleAll();
        page.tasks.filter(visible).shouldBe(empty);
        page.assertItemsLeftCounter(0);
    }

    @Test
    public void testCompletedFilter() {

        // Precondition
        page.addTask("a");
        page.addTask("b");
        page.addTask("c");
        page.toggleTask("c");
        page.tasks.filter(visible).shouldHave(exactTexts("a", "b", "c"));

        // Only completed items are visible
        page.filterCompleted();
        page.tasks.filter(visible).shouldHave(exactTexts("c"));
        page.assertItemsLeftCounter(2);

        // Toggled task in Active also is visible in Completed
        page.filterActive();
        page.addTask("d from completed");
        page.toggleTask("d from completed");
        page.filterCompleted();
        page.tasks.filter(visible).shouldHave(texts("c", "d from completed"));
        page.assertItemsLeftCounter(2);

        // Deleting by X
        page.destroyTask("c");
        page.tasks.filter(visible).shouldHave(exactTexts("d from completed"));
        page.assertItemsLeftCounter(2);

        // Deleting by clear-completed
        page.clearCompleted();
        page.tasks.filter(visible).shouldBe(empty);
        page.assertItemsLeftCounter(2);

        // There are not deleted items in All
        page.filterAll();
        page.tasks.shouldHave(exactTexts("a", "b"));
        page.assertItemsLeftCounter(2);
    }
}