package TodoTesting.Homework_3.pageobjects.pages;

import com.codeborne.selenide.ElementsCollection;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class TodoMVCPage {

    public ElementsCollection tasks = $$("#todo-list li");

    @Step
    public void addTask(String text) {
        $("#new-todo").val(text).pressEnter();
    }

    @Step
    public void toggleTask(String taskText) {
        tasks.find(text(taskText)).find(".toggle").click();
    }

    @Step
    public void filterAll(){ $(By.linkText("All")).click(); }

    @Step
    public void filterActive(){ $(By.linkText("Active")).click(); }

    @Step
    public void filterCompleted(){ $(By.linkText("Completed")).click(); }

    @Step
    public void assertItemsLeftCounter(int itemsNumber) {
        $("#todo-count>strong").shouldHave(exactText(String.valueOf(itemsNumber)));
    }

    @Step
    public void clearCompleted() {
        $("#clear-completed").click();
    }

    @Step
    public void toggleAll() {
        $("#toggle-all").click();
    }

    @Step
    public void editTask(String oldText, String newText) {
        tasks.find(exactText(oldText)).find("label").doubleClick();
        tasks.find(cssClass("editing")).find(".edit").setValue(newText).pressEnter();
    }

    @Step
    public void destroyTask(String destroyText) {
        tasks.find(exactText(destroyText)).hover().find(".destroy").click();
    }
}
